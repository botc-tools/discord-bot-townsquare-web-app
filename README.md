# Project Description

This tool is a web app that connects Discord updates with other details that you would otherwise set in the Online Townsquare tool. The web app allows you to define player roles & town square seats for each user in Discord. These values are stored locally in a JS object. You will need to hook this data into some other tool in order to make it useful.

# Deployment

This app was designed to run on Heroku or Dokku. All of the dependencies are defined in the Pipenv file. The Procfile will start the app if it is deployed into a Heroku like enviroment.

# Enviroment Vars

The app assumes your enviroment has two variables set. Your appearch to setting these will change based on your enviroment. If you are running locally, you can achieve this by creating a file in the root directory called `.env` with the following info:

```
BOT_TOKEN=<DISCORD BOT TOKEN>
WEB_HOSTNAME=http://localhost:5000
```

(You'll obviously replace <DISCORD BOT TOKEN> with your bot token, and http://localhost:5000 with your webpage domain).

# Pieces

There is a discord bot and a Flask web app. The Discord bot is just sendin VoiceState updates to the Flask App with a POST request.

## Discord Bot

You can start the discord bot with `pipenv run python bot` from the root directory.

## Flask App

You can start the web app with `pipenv run python main.py` from the root directory.

Running this on a webserver requires eventlet in order to manage the websockets. _Note: The guinicorn + eventlet currently have a production issue that prevents them from working. I am using an older version of eventlet that have a known DOS vulnerability. This should be changed later._

