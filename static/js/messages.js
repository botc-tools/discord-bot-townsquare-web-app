
// Messages from the Discord bot
var socket = io(HOSTNAME);
socket.on('discord_update', function(msg) {
    addDiscordServer(msg.guild.id, msg.guild.name);
    var selected_server = document.querySelector("select#discord_server option:checked").value;
    if (selected_server == "" || msg.guild.id.toString() == selected_server) {
        updateDiscordParticipantList(msg.channels);
    }
});

// Set values based on Discord updates
function addDiscordServer(id, name) {
    var options = document.querySelectorAll("select#discord_server option");
    var servers = [];
    for (var s=0; s < options.length; s++) {
        servers.push(options[s].value);
    }
    if (!servers.includes(id.toString())) {
        let element = document.createElement("option");
        element.setAttribute("value", id);
        element.textContent = name + " (" + id + ")";
        document.querySelector("select#discord_server").appendChild(element);
    }
}

var activeDiscordGuild = null;
function setDiscordGuild(guild_id) {
    activeDiscordGuild = guild_id;
}

function updateDiscordParticipantList(vc_list) {
    for (var c=0; c < vc_list.length; c++) {
        for (var p=0; p < vc_list[c].members.length; p++) {
            var participant = vc_list[c].members[p];
            addParticipant(participant.id, participant.display_name, { 'id': vc_list[c].id, 'name': vc_list[c].name });
        }
    }
}

