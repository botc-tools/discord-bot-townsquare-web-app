var participants = {};

function addParticipant(ds_id, ds_display_name, channel) {
    if (ds_id in participants) {
        participants[ds_id].display_name = ds_display_name;
        participants[ds_id].channel = channel;
    } else {
        var existing = JSON.parse(window.localStorage.getItem('participant_' + ds_id));
        participants[ds_id] = {
            "id": ds_id,
            "display_name": ds_display_name,
            "role": existing ? existing["role"] : null,
            "seat": existing ? existing["seat"] : null,
            "channel": channel
        }
    }
    updateParticipantRow(ds_id);
}

function setParticipant(ds_id, display_name, role, seat, channel) {
    if (!display_name)  { display_name = participants[ds_id].display_name; }
    if (!channel)       { channel = participants[ds_id].channel; }
    if (!role)          { role = participants[ds_id].role; }
    if (!seat)          { seat = participants[ds_id].seat; }
    if (!roles.includes(role) && !["storyteller","griminion"].includes(role)) { role = null; }

    participants[ds_id].display_name = display_name;
    participants[ds_id].channel = channel;
    participants[ds_id].role = role;
    participants[ds_id].seat = seat;

    updateParticipantRow(ds_id);
}

function participantRow(ds_id) {
    var rows = document.querySelectorAll("#participant_table tr[id^='participant_']");
    for (var r=0; r < rows.length; r++) {
        if (rows[r].id == "participant_" + ds_id) {
            return rows[r];
        }
    }
    var row = document.querySelector("#participant_table tbody").insertRow(-1);
    row.id = "participant_" + ds_id;
    row.insertCell(-1);
    row.insertCell(-1);
    row.insertCell(-1);
    row.insertCell(-1);
    row.insertCell(-1);
    return row;
}

function buildRoleSelector(ds_id, roles) {
    let selector = document.createElement("select");

    // defaults
    var defaults = [[null, "-- Unassigned --"],["storyteller", "-- Storyteller --"],["griminion","-- Griminion --"]];
    for (var d=0; d < defaults.length; d++) {
        var option = document.createElement("option");
        option.value = defaults[d][0];
        option.text = defaults[d][1];
        if (defaults[d][0] == participants[ds_id].role) { option.selected = 'selected'; }
        selector.add(option);
    }

    // script roles
    for (var r=0; r < roles.length; r++) {
        let option = document.createElement("option");
        option.value = roles[r];
        option.text = roles[r].replace("_"," ").replace(/(^\w{1})|(\s+\w{1})/g, letter => letter.toUpperCase()); // capitalize each word
        if (roles[r] == participants[ds_id].role) { option.selected = 'selected'; }
        selector.add(option);
    }
    selector.dataset.ds_id = ds_id;
    selector.onchange = function(){ assignRole(this.dataset.ds_id, this.value); };
    return selector;
}

function assignRole(ds_id, role) {
    setParticipant(ds_id, null, role, null, null);
    if (role == "storyteller") {
        ChangeStoryteller(participants[ds_id]);
    } else if (role == "griminion") {
        ChangeGriminion(participants[ds_id]);
    }
}

function buildSeatSelector(ds_id) {
    let selector = document.createElement("select");

    // defaults
    var defaults = [[null, "-- Unseated --"]];
    for (var d=0; d < defaults.length; d++) {
        var option = document.createElement("option");
        option.value = defaults[d][0];
        option.text = defaults[d][1];
        selector.add(option);
    }

    // normal seats
    for (var s=1; s <= 20; s++) {
        let option = document.createElement("option");
        option.value = s;
        option.text = s;
        if (s == participants[ds_id].seat) { option.selected = 'selected'; }
        selector.add(option);
    }
    selector.dataset.ds_id = ds_id;
    selector.onchange = function(){ setParticipant(this.dataset.ds_id, null, null, this.value, null); };
    return selector;
}

function buildChannelName(ds_id) {
    if (participants[ds_id].channel) { 
        return participants[ds_id].channel.name;
    }
    return "--";
}

function updateParticipantRow(ds_id) {
    var row = participantRow(ds_id);
    row.children[0].replaceChildren(participants[ds_id].display_name);
    row.children[1].replaceChildren(buildChannelName(ds_id));
    row.children[2].replaceChildren(buildRoleSelector(ds_id, roles));
    row.children[3].replaceChildren(buildSeatSelector(ds_id));
    window.localStorage.setItem(
        'participant_' + ds_id, 
        JSON.stringify(participants[ds_id])
    );
}

