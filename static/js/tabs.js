function changeTab(tab) {
    if (tab == "script") {
        document.querySelector("#script").style.display = "block";
        document.querySelector("#participants").style.display = "none";
    } else if (tab == "participants") {
        document.querySelector("#script").style.display = "none";
        document.querySelector("#participants").style.display = "block";
    }
}

