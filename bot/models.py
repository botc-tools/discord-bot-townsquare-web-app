class Guild():
    def __init__(self, guild, **kwargs):
        self.id = guild.id
        self.name = guild.name

    def __str__(self):
        return '<Guild id={} name="{}">'.format(self.id, self.name)

    def __repr__(self):
        return '{ "id": {} "name": "{}" }'.format(self.id, self.name)

class VoiceChannel():
    def __init__(self, channel, **kwargs):
        self.id = channel.id
        self.name = channel.name
        self.position = channel.position
        self.members = [Member(member) for member in channel.members]

    def __str__(self):
        return '<VoiceChannel id={} name="{}" members={}>'.format(self.id, self.name, self.members)

    def __repr__(self):
        return '{ "id": {} "name": "{}" "members": {} }'.format(self.id, self.name, self.members)

class Member():
    def __init__(self, member, **kwargs):
        self.id = member.id
        self.name = member.name
        self.nickname = member.nick
        self.display_name = member.display_name

    def __str__(self):
        return '<User id={} display_name="{}">'.format(self.id, self.display_name)

    def __repr__(self):
        return '{ "id": {} "display_name": "{}" }'.format(self.id, self.display_name)


