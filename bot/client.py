import discord
import os

import channels

bot_token = os.environ['BOT_TOKEN']

class DiscordClient(discord.Client):
    async def on_ready(self):
        for guild in self.guilds:
            print("Connected to {}".format(guild.id))

    async def on_voice_state_update(self, member, before, after):
        if after.channel:
            await channels.push_channel_update(after.channel.guild)

