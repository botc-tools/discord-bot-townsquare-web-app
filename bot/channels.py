import os
import requests

import models

ui_client = os.environ['WEB_HOSTNAME']

async def build_channel_list(guild):
    channels = [models.VoiceChannel(channel).__dict__ for channel in guild.voice_channels]
    for channel in channels:
        channel['members'] = [member.__dict__ for member in channel['members']]
    return {
        'guild': models.Guild(guild).__dict__,
        'channels': channels
    }

async def push_channel_update(guild):
    channels = await build_channel_list(guild)
    x = requests.post(ui_client + "/api/channels", json=channels)
    print(x)

