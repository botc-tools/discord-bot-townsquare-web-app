from flask import Flask, render_template, jsonify, request
from flask_socketio import SocketIO

import requests
import json
import os

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app)

@app.route("/")
def index():
    return render_template('index.html', HOSTNAME=os.environ['WEB_HOSTNAME'])

@app.route('/api/channels', methods=['POST']) 
def api_channels():
    data = request.json
    send_discord_update(data)
    return json.dumps({'success':True}), 200, {'ContentType':'application/json'}  

def send_discord_update(data):
    socketio.emit('discord_update', data, broadcast=True)

if __name__ == '__main__':
    socketio.run(app, port=5000)

